from django.shortcuts import render
from .models import Book
from .forms import NewForm, EditForm
from django.shortcuts import redirect, get_object_or_404


def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def create_view(request):
    context = {}
    form = NewForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect('show_book_details', pk=book.pk)
    context ['form'] = form
    return render(request, "books/create.html", context)


def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, "books/detail.html", context)


def edit_view(request, pk):
    context ={}
    obj = get_object_or_404(Book, pk = pk)
    form = EditForm(request.POST or None, instance = obj)
    if form.is_valid():
        book=form.save()
        return redirect("show_book_details", pk=book.pk)
    context['form']= form
    return render(request, "books/edit.html", context) 


def delete_book(request, pk):
	context ={}
	obj = get_object_or_404(Book, pk = pk)
	if request.method == "POST":		
		obj.delete()
		return redirect("show_books")
	return render(request, "books/delete.html", context)
    
