from django.contrib import admin
from books.models import Book, BookReview

# Register your models here.
class BookAdmin(Book):
    pass
admin.site.register(Book)
admin.site.register(BookReview)
