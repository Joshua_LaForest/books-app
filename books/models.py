from django.db import models
from django.contrib.auth.models import User




class Author(models.Model):
    name = models.CharField(max_length=20, unique=True)
    
    def __str__(self):
        return self.name

class Book(models.Model):
    title  = models.CharField(max_length = 30, unique = True)
    author = models.ManyToManyField(Author, related_name = "books")
    pages = models.SmallIntegerField(null = True, blank = True)
    isbn = models.BigIntegerField(null = True, blank = True)
    description = models.TextField(null = True, blank = True)
    published_in = models.SmallIntegerField(null = True, blank = True)
    in_print = models.BooleanField(null = True, blank = True)
    image =  models.URLField(null = True, blank = True)
        
    def __str__(self):
        return self.title + " by " + self.author
    
class BookReview(models.Model):
    #reviewer = models.ForeignKey(User, related_name = "reviews", on_delete = models.CASCADE)
    book = models.ForeignKey(Book, related_name = "reviews", on_delete= models.CASCADE)
#class Create_view(models.Model):
#    title = models.CharField(max_length = 50, null = True)
#    author  = models.CharField(max_length = 200, null = True)
    
 #   def __str__(self):
  #      return self.title
