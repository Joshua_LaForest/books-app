from django.urls import path, include
from django.contrib import admin
from books.views import(show_books)
from books.views import(create_view)
from books.views import(show_book_details)
from books.views import(edit_view)
from books.views import(delete_book)
#from books.views import(show_detail)


urlpatterns = [
    path("", show_books, name = "show_books"),
    path("create/", create_view, name = "create_book"),
    path("<int:pk>/", show_book_details, name = "show_book_details"),
    path("<int:pk>/edit/", edit_view, name = "edit_view"),
    path("<int:pk>/delete", delete_book, name = "delete_book"),
]
