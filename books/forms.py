from django import forms
from .models import Book

class NewForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "title",
            "author", 
            "pages",
            "isbn",
            "description",
            "published_in",
            "in_print",
            "image",
        ]
        #alternatly instead of fields  use exclude = []
        
class EditForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "description",
            "image",
        ]