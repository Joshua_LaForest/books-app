from django import forms
from magazine.models import Magazine, Issue, Genre


class NewForm(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []
        
class IssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        exclude = []


class GenreForm(forms.ModelForm):
    class Meta:
        model = Issue
        exclude = []

