from django.contrib import admin
from .models import Magazine
from .models import Issue
from .models import Genre
# from .models import Review


admin.site.register(Magazine)
admin.site.register(Issue)
admin.site.register(Genre)
# admin.site.register(Review)
