from django.shortcuts import get_object_or_404, render
from magazine.models import Magazine
from django.shortcuts import redirect
from magazine.forms import NewForm
from magazine.models import Genre
# from magazine.models import Review
# from django.contrib.auth.decorators import login_required

# Create your views here.
def show_mags(request):
    magazines = Magazine.objects.all()
    context = {
        'magazines': magazines
    }
    return render(request, "magazine/list_reviews.html", context)


#@login_required
#def list_reviews(request):
    #magazine_reviews_by_this_user = request.user.reviews_by_this_user.all()
    #context = {
        #"reviews_queryset": magazine_reviews_by_this_user
    #}
    
    #return render(request, "magazine/list.html", context)



def create_mag(request):
    context = {}
    form = NewForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_mags")
    context ['form'] = form
    return render(request, "magazine/create.html", context)


def view_mag(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        'magazine': magazine
    }
    return render(request, "magazine/details.html", context)


def delete_mag(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk = pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_mags")
    return render(request, "magazine/delete.html", context)


def edit_mag(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk = pk)
    form = NewForm(request.POST or None, instance = obj)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_mags")
    context['form']= form
    return render(request, "magazine/edit.html", context) 

def mags_genre(request, pk):
    genre = Genre.objects.all(pk=pk)
    context = {
        'genre': genre
    }
    return render(request, "magazine/genre_details.html", context)