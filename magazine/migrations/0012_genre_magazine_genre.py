# Generated by Django 4.0.6 on 2022-07-22 00:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magazine', '0011_remove_magazine_genre_delete_genre'),
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=200, null=True)),
                ('magazine', models.ManyToManyField(related_name='genres', to='magazine.magazine')),
            ],
        ),
        migrations.AddField(
            model_name='magazine',
            name='genre',
            field=models.ManyToManyField(related_name='magazines', to='magazine.genre'),
        ),
    ]
