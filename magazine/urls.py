from django.urls import path
from django.contrib import admin
from magazine.views import show_mags
from magazine.views import create_mag
from magazine.views import view_mag
from magazine.views import delete_mag
from magazine.views import edit_mag
from magazine.views import mags_genre
#from magazine.views import list_reviews


urlpatterns = [
    path('', show_mags, name = "show_mags"),
    path("create/", create_mag, name = "create_mag"),
    path("<int:pk>/", view_mag, name = "view_mag"),
    path("<int:pk>/delete", delete_mag, name = "delete_mag"),
    path("<int:pk>/edit/", edit_mag, name = "edit_mag"),
    path("genre/<int:pk>/", mags_genre, name = "mags_genre"),
    #path('reviews/', list_reviews, name = "list_reviews"),
]