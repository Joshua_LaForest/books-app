from django.db import models
from django.contrib.auth.models import User
# class Issue(models.Model):
#     title = models.CharField(max_length=20)
    

class Magazine(models.Model):
    title = models.CharField(max_length=40, blank = True)
    # issue_title = models.ManyToManyField(Issue, related_name = "magazine")
    release_cycle = models.CharField(max_length=20, blank = True)
    description = models.TextField(null = True, blank = True)
    cover_image = models.URLField(null = True, blank = True)
    # page_count = models.CharField(null = True, blank = True)
    genre = models.ManyToManyField("Genre", related_name = 'magazines')
    creator = models.ForeignKey(User, related_name = 'magazine', on_delete = models.CASCADE)
    
    
    
    def __str__(self):
        return self.title 
    
    
class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    title = models.CharField(max_length=50, unique=True, blank=True)
    date_published = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_issue_image = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Genre(models.Model):
    name = models.CharField(max_length=200, null = True, blank = True)
    magazine = models.ManyToManyField(Magazine, related_name='genres')
    
    def __str__(self):
        return self.name
    
    
# class Review(models.Model):
#     magazine = models.ForeignKey(Magazine, related_name = "reviews", on_delete = models.CASCADE)
#     reviewer = models.ForeignKey(User, related_name = "reviews_by_this_user", on_delete = models.CASCADE)
#     description = models.TextField(null = True, blank = True)